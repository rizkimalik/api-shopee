'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const response = require('../helper/json_response');

const data_customers = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter } = req.query;
        const dataskip = skip === undefined ? 0 : skip;
        const datatake = take === undefined ? 15 : take;
        const datasort = sort === undefined ? '' : JSON.parse(sort);
        const datafilter = filter === undefined ? '' : filter;

        let orderby = '';
        if (datasort[0] === undefined) {
            orderby = `ORDER BY CustomerID ASC`;
        }
        else {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter !== '') {
            String.prototype.replaceAll = function (search, replacement) {
				let target = this;
				return target.split(search).join(replacement);
			}

            const data1 = datafilter.replaceAll('[', '');
            const data2 = data1.replaceAll(']', '');
            const data3 = data2.replaceAll(',"contains",', ':');
            const data4 = data3.replaceAll(',"or",', ',');
            const data5 = JSON.parse(`{${data4}}`);

            filtering = `WHERE Name LIKE '%${data5.Name}%' 
            OR CustomerID LIKE '%${data5.CustomerID}%' 
            OR Email LIKE '%${data5.Email}%'
            OR Alamat LIKE '%${data5.Alamat}%'
            OR HP LIKE '%${data5.HP}%'
            OR NIK LIKE '%${data5.NIK}%'
            OR CIF LIKE '%${data5.CIF}%'
            OR GroupID LIKE '%${data5.GroupID}%'`;
        }

        // const customers = await knex.raw(`EXEC SP_CustomerData '${skip}','${take}', '${jsonsort}', ''`);
        const customers = await knex.raw(`
            SELECT CustomerID,Name,Birth,JenisKelamin,Email,Alamat,HP,NIK,CIF,GroupID
            FROM mCustomer 
            ${filtering}
            ${orderby}
            OFFSET ${dataskip} ROWS FETCH NEXT ${datatake} ROWS ONLY
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from mCustomer ${filtering}`);
        res.json({
            'data': customers,
            'totalCount': total[0].total
        });
    }
    catch (error) {
        response.error(res, error, 'customer/index');
    }
}

const insert_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const now = new Date();
        const CustomerID = date.format(now, 'YYMMDDHHmmSS');
        const {
            Name,
            Birth,
            JenisKelamin,
            Email,
            Alamat,
            HP,
            NIK,
            CIF,
            StatusUpdated = 'No',
            Status = 'REGISTER',
            AccountID = '0',
            UserCreate,
            DateCreate = knex.fn.now()
        } = req.body;

        if (Email != undefined) {
            const check_email = await knex('mCustomer').count('CustomerID as JML').where({ Email }).first();
            if (check_email.JML > 0) return response.created(res, `Email : ${Email} - already exists.`);

            const channel_email = await knex('SML_mCustomerChannel').count('CustomerID as JML').where({ ValueChannel: Email }).first();
            if (channel_email.JML === 0) {
                await knex('SML_mCustomerChannel')
                    .insert([{
                        CustomerID,
                        ValueChannel: Email,
                        FlagChannel: 'Email',
                        UserCreate,
                        Status: 'N'
                    }]);
            }
        }
        if (HP != undefined) {
            const check_phone = await knex('mCustomer').count('CustomerID as JML').where({ HP }).first();
            if (check_phone.JML > 0) return response.created(res, `Phone : ${HP} - already exists.`);

            const channel_phone = await knex('SML_mCustomerChannel').count('CustomerID as JML').where({ ValueChannel: HP }).first();
            if (channel_phone.JML === 0) {
                await knex('SML_mCustomerChannel')
                    .insert([{
                        CustomerID,
                        ValueChannel: HP,
                        FlagChannel: 'Phone',
                        UserCreate,
                        Status: 'N'
                    }]);
            }
        }
        if (NIK != undefined) {
            const check_nik = await knex('mCustomer').count('CustomerID as JML').where({ NIK }).first();
            if (check_nik.JML > 0) return response.created(res, `NIK : ${NIK} - already exists.`);
        }
        if (CIF != undefined) {
            const check_CIF = await knex('mCustomer').count('CustomerID as JML').where({ CIF }).first();
            if (check_CIF.JML > 0) return response.created(res, `CIF : ${CIF} - already exists.`);
        }

        await knex('mCustomer')
            .insert([{
                CustomerID,
                Name,
                Birth,
                JenisKelamin,
                Email,
                HP,
                Alamat,
                NIK, CIF, StatusUpdated, Status, AccountID,
                UserCreateCustomer: UserCreate,
                DateCreateCustomer: DateCreate
            }]);
        const getcustomer = await knex('mCustomer').where({ CustomerID }).first();
        response.ok(res, getcustomer);
    }
    catch (error) {
        response.error(res, error, 'customer/insert_customer');
    }
}

const update_customer = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const CustomerID = data.CustomerID;
        const values = data.values;
        values.DateUpdate = knex.fn.now();

        if (values.Email != undefined) {
            const check_email = await knex('mCustomer').count('CustomerID as JML').where({ Email: values.Email }).whereNot({ CustomerID }).first();
            if (check_email.JML > 0) return response.created(res, `email : ${values.Email} - already exists.`);

            const channel_email = await knex('SML_mCustomerChannel').count('CustomerID as JML').where({ ValueChannel: values.Email }).first();
            if (channel_email.JML === 0) {
                await knex('SML_mCustomerChannel')
                    .insert([{
                        CustomerID,
                        ValueChannel: values.Email,
                        FlagChannel: 'Email',
                        UserCreate: values.UserUpdate,
                        Status: 'N'
                    }]);
            }
        }
        if (values.HP != undefined) {
            const check_phone = await knex('mCustomer').count('CustomerID as JML').where({ HP: values.HP }).whereNot({ CustomerID }).first();
            if (check_phone.JML > 0) return response.created(res, `telephone : ${values.HP} - already exists.`);

            const channel_phone = await knex('SML_mCustomerChannel').count('CustomerID as JML').where({ ValueChannel: values.HP }).first();
            if (channel_phone.JML === 0) {
                await knex('SML_mCustomerChannel')
                    .insert([{
                        CustomerID,
                        ValueChannel: values.HP,
                        FlagChannel: 'Phone',
                        UserCreate: values.UserUpdate,
                        Status: 'N'
                    }]);
            }
        }
        if (values.NIK != undefined) {
            const check_nik = await knex('mCustomer').count('CustomerID as JML').where({ NIK: values.NIK }).whereNot({ CustomerID }).first();
            if (check_nik.JML > 0) return response.created(res, `NIK : ${values.NIK} - already exists.`);
        }
        if (values.CIF != undefined) {
            const check_CIF = await knex('mCustomer').count('CustomerID as JML').where({ CIF: values.CIF }).whereNot({ CustomerID }).first();
            if (check_CIF.JML > 0) return response.created(res, `CIF : ${values.CIF} - already exists.`);
        }

        await knex('mCustomer')
            .update(values)
            .where({ CustomerID });
        const getcustomer = await knex('mCustomer').where({ CustomerID }).first();
        response.ok(res, getcustomer);
    }
    catch (error) {
        response.error(res, error, 'customer/update_customer');
    }
}

const delete_customer = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end();
        const { CustomerID } = req.body;
        const delData = await knex('mCustomer').where({ CustomerID }).del();
        response.ok(res, delData);
    }
    catch (error) {
        response.error(res, error, 'customer/delete_customer');
    }
}

module.exports = {
    data_customers,
    insert_customer,
    update_customer,
    delete_customer,
}
