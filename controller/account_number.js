'use strict';
const knex = require('../config/db_connect');
// const date = require('date-and-time');
const response = require('../helper/json_response');

const data_accountnumber = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { CustomerID } = req.params;
        const account_number = await knex('BTN_NomorRekening').select('ID', 'CustomerID', 'NomorRekening').where({ CustomerID });
        response.ok(res, account_number);
    }
    catch (error) {
        response.error(res, error, 'account_number/data');
    }
}

const insert_accountnumber = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            CustomerID,
            NomorRekening,
            Usercreate,
            Datecreate = knex.fn.now()
        } = req.body;

        await knex('BTN_NomorRekening')
            .insert([{
                CustomerID,
                NomorRekening,
                Usercreate,
                Datecreate
            }]);
        response.ok(res, 'success insert data.');
    }
    catch (error) {
        response.error(res, error, 'account_number/insert');
    }
}

const update_accountnumber = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const ID = data.ID;
        const values = data.values;

        await knex('BTN_NomorRekening')
            .update(values)
            .where({ ID });
        response.ok(res, 'success update data.');
    }
    catch (error) {
        response.error(res, error, 'account_number/update');
    }
}

const delete_accountnumber = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end();
        const { ID } = req.body;
        const delData = await knex('BTN_NomorRekening').where({ ID }).del();
        response.ok(res, delData);
    }
    catch (error) {
        response.error(res, error, 'account_number/delete');
    }
}

module.exports = {
    data_accountnumber,
    insert_accountnumber,
    update_accountnumber,
    delete_accountnumber,
}