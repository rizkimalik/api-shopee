'use strict';
const knex = require('../config/db_connect');
const response = require('../helper/json_response');

const update_group = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { FromCustomerID, ToCustomerID, UserCreate } = req.body;
        const update_group = await knex.raw(`EXEC TR_GroupingCustomer '${FromCustomerID}','${ToCustomerID}','${UserCreate}'`);
        response.ok(res, update_group);
    }
    catch (error) {
        response.error(res, error, 'customer_group/update_group');
    }
}

const delete_group = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        const { FromCustomerID, UserCreate } = req.body;
        const delete_group = await knex.raw(`EXEC TR_DeleteGroupingCustomer '${FromCustomerID}','${UserCreate}'`);
        response.ok(res, delete_group);
    }
    catch (error) {
        response.error(res, error, 'customer_group/delete_group');
    }
}

module.exports = {
    update_group,
    delete_group,
}