'use strict';
const knex = require('../config/db_connect');
// const date = require('date-and-time');
const response = require('../helper/json_response');

const all_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter } = req.query;
        const dataskip = skip === undefined ? 0 : skip;
        const datatake = take === undefined ? 15 : take;
        const datasort = sort === undefined ? '' : JSON.parse(sort);
        const datafilter = filter === undefined ? '' : filter;

        let orderby = '';
        if (datasort[0] === undefined) {
            orderby = `ORDER BY CustomerID ASC`;
        }
        else {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter !== '') {
            String.prototype.replaceAll = function (search, replacement) {
				let target = this;
				return target.split(search).join(replacement);
			}

            const data1 = datafilter.replaceAll('[', '');
            const data2 = data1.replaceAll(']', '');
            const data3 = data2.replaceAll(',"contains",', ':');
            const data4 = data3.replaceAll(',"or",', ',');
            const data5 = JSON.parse(`{${data4}}`);

            filtering = `WHERE CustomerID LIKE '%${data5.CustomerID}%' 
            OR FlagChannel LIKE '%${data5.FlagChannel}%'
            OR ValueChannel LIKE '%${data5.ValueChannel}%'
            OR Name LIKE '%${data5.Name}%'`;
        }

        const all_channel = await knex.raw(`
            select * from (
                SELECT a.CustomerID,a.FlagChannel,a.ValueChannel,b.Name
                        FROM SML_mCustomerChannel a 
                        left outer join mCustomer b on a.CustomerID=b.CustomerID  
            ) as all_channel
            ${filtering}
            ${orderby}
        `);
        // const all_channel = await knex.raw(`
        //     select CustomerID, Name from (
        //         SELECT a.CustomerID,a.FlagChannel,a.ValueChannel,b.Name
        //             FROM SML_mCustomerChannel a 
        //             left outer join mCustomer b on a.CustomerID=b.CustomerID 
        //         ${filtering}
        //     ) as all_channel
        //     Group by CustomerID, Name
        //     ${orderby}
        //     OFFSET ${dataskip} ROWS FETCH NEXT ${datatake} ROWS ONLY
        // `);
        
        const total = await knex.raw(`
            select COUNT(*) AS total from (
                SELECT a.CustomerID,a.FlagChannel,a.ValueChannel,b.Name
                    FROM SML_mCustomerChannel a 
                    left outer join mCustomer b on a.CustomerID=b.CustomerID  
            ) as all_channel 
            ${filtering}
        `);
        res.json({
            'data': all_channel,
            'totalCount': total[0].total
        });
    }
    catch (error) {
        response.error(res, error, 'channel/all_channel');
    }
}

const data_channel = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { CustomerID } = req.params;
        const data_channel = await knex('SML_mCustomerChannel').select('ID', 'CustomerID', 'FlagChannel', 'ValueChannel').where({ CustomerID });
        response.ok(res, data_channel);
    }
    catch (error) {
        response.error(res, error, 'channel/data');
    }
}


const insert_channel = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            CustomerID,
            FlagChannel,
            ValueChannel,
            UserCreate,
            DateCreate = knex.fn.now()
        } = req.body;

        await knex('SML_mCustomerChannel')
            .insert([{
                CustomerID,
                FlagChannel,
                ValueChannel,
                UserCreate,
                DateCreate
            }]);
        response.ok(res, 'success insert data.');
    }
    catch (error) {
        response.error(res, error, 'channel/insert');
    }
}

const update_channel = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const data = req.body;
        const ID = data.ID;
        const values = data.values;

        await knex('SML_mCustomerChannel')
            .update(values)
            .where({ ID });
        response.ok(res, 'success update data.');
    }
    catch (error) {
        response.error(res, error, 'channel/update');
    }
}


const delete_channel = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end();
        const { ID } = req.body;
        const delData = await knex('SML_mCustomerChannel').where({ ID }).del();
        response.ok(res, delData);
    }
    catch (error) {
        response.error(res, error, 'channel/delete');
    }
}

module.exports = {
    all_channel,
    data_channel,
    insert_channel,
    update_channel,
    delete_channel,
}