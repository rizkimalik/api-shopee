'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const response = require('../helper/json_response');

const data_history = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter, user_create } = req.query;
        const dataskip = skip === undefined ? 0 : skip;
        const datatake = take === undefined ? 15 : take;
        const datasort = sort === undefined ? '' : JSON.parse(sort);
        const datafilter = filter === undefined ? '' : filter;

        let orderby = '';
        if (datasort[0] === undefined) {
            orderby = `ORDER BY DateCreate DESC`;
        }
        else {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter !== '') {
            String.prototype.replaceAll = function (search, replacement) {
                let target = this;
                return target.split(search).join(replacement);
            }

            const data1 = datafilter.replaceAll('[', '');
            const data2 = data1.replaceAll(']', '');
            const data3 = data2.replaceAll(',"contains",', ':');
            const data4 = data3.replaceAll(',"or",', ',');
            const data4a = data4.replaceAll(',"=",', ':');
            const data5 = JSON.parse(`{${data4a}}`);

            filtering += 'AND ';
            if (data5.TicketNumber !== undefined) {
                filtering += `A.TicketNumber LIKE '%${data5.TicketNumber}%'`;
            } else if (data5.TicketSourceName !== undefined) {
                filtering += `A.TicketSourceName LIKE '%${data5.TicketSourceName}%'`;
            } else if (data5.CustomerID !== undefined) {
                filtering += `C.CustomerID LIKE '%${data5.CustomerID}%'`;
            } else if (data5.Name !== undefined) {
                filtering += `C.Name LIKE '%${data5.Name}%'`;
            } else if (data5.EMAIL !== undefined) {
                filtering += `C.EMAIL LIKE '%${data5.EMAIL}%'`;
            } else if (data5.GenesysID !== undefined) {
                filtering += `A.GenesysID LIKE '%${data5.GenesysID}%'`;
            } else if (data5.NomorRekening !== undefined) {
                filtering += `A.NomorRekening LIKE '%${data5.NomorRekening}%'`;
            } else if (data5.ThreadID !== undefined) {
                filtering += `A.ThreadID LIKE '%${data5.ThreadID}%'`;
            } else if (data5.ThreadTicket !== undefined) {
                filtering += `A.ThreadTicket LIKE '%${data5.ThreadTicket}%'`;
            } else if (data5.HP !== undefined) {
                filtering += `C.HP LIKE '%${data5.HP}%'`;
            } else if (data5.alamat !== undefined) {
                filtering += `C.alamat LIKE '%${data5.alamat}%'`;
            } else if (data5.NIK !== undefined) {
                filtering += `C.NIK LIKE '%${data5.NIK}%'`;
            } else if (data5.CIF !== undefined) {
                filtering += `C.CIF LIKE '%${data5.CIF}%'`;
            } else if (data5.AccountInbound !== undefined) {
                filtering += `A.AccountInbound LIKE '%${data5.AccountInbound}%'`;
            } else if (data5.SubCategory3Name !== undefined) {
                filtering += `A.SubCategory3Name LIKE '%${data5.SubCategory3Name}%'`;
            } else if (data5.DetailComplaint !== undefined) {
                filtering += `A.DetailComplaint LIKE '%${data5.DetailComplaint}%'`;
            } else if (data5.ResponComplaint !== undefined) {
                filtering += `A.ResponComplaint LIKE '%${data5.ResponComplaint}%'`;
            } else if (data5.Status !== undefined) {
                filtering += `A.Status LIKE '%${data5.Status}%'`;
            } else if (data5.ParentNumberID !== undefined) {
                filtering += `A.ParentNumberID LIKE '%${data5.ParentNumberID}%'`;
            } else if (data5.UserCreate !== undefined) {
                filtering += `A.UserCreate LIKE '%${data5.UserCreate}%'`;
            } else if (data5.DateCreate !== undefined) {
                filtering += `A.DateCreate = '${data5.DateCreate}'`;
            } else if (data5.LastResponseBy !== undefined) {
                filtering += `A.LastResponseBy LIKE '%${data5.LastResponseBy}%'`;
            } else if (data5.LastResponseDate !== undefined) {
                filtering += `A.LastResponseDate = '${data5.LastResponseDate}'`;
            }
        }

        const setting = await knex('Temp_SettingHiStoryTransaction').select('DAY AS JmlDay', 'FilterDate').where('CreatedBy', user_create).andWhere('Type', 'History').first();

        if (setting !== undefined) {
            const filter_date = setting.FilterDate === null ? 'GETDATE()' : `'${date.format(setting.FilterDate, 'YYYY-MM-DD')}'`;
            const jml_day = setting.JmlDay === null ? 0 : setting.JmlDay;
            const firstdate = `DateAdd(DD,-${jml_day}, CONVERT(char(10), ${filter_date},126))`;
            const lastdate = `CONVERT(char(10), ${filter_date},126)`;

            const data_history = await knex.raw(`
                SELECT a.DetailComplaint,a.ResponComplaint,
                A.TicketNumber,A.AccountInbound, A.SubCategory3Name, A.Status, A.TicketSourceName, A.UserCreate, A.NomorRekening, 
                CONVERT(nvarchar,A.DateCreate,120) as DateCreate,CONVERT(nvarchar,A.DateClose,120) as DateClose, a.GenesysID, a.ThreadID, a.ThreadTicket,  a.LastResponseBy, a.LastResponseDate,
                a.ParentNumberID, a.ParentNumberCreated, a.ParentNumberDate, a.ParentReason,
                C.CustomerID,C.Name, C.HP, C.CIF, C.NIK, C.EMAIL, c.alamat
                FROM TTICKET A LEFT OUTER JOIN MCUSTOMER C ON A.NIK = C.CUSTOMERID
                WHERE A.DateCreate BETWEEN ${firstdate} AND ${lastdate} + ' 23:59'
				${filtering}
                ${orderby}
                OFFSET ${dataskip} ROWS FETCH NEXT ${datatake} ROWS ONLY
            `);
            const total = await knex.raw(`
                SELECT COUNT(*) AS total FROM TTICKET A LEFT OUTER JOIN MCUSTOMER C ON A.NIK = C.CUSTOMERID 
                WHERE A.DateCreate BETWEEN ${firstdate} AND ${lastdate} ${filtering}
            `);
			
			

            res.json({
                'data': data_history,
                'totalCount': total[0].total
            });
        }
        else {
            res.json({
                'data': [],
                'totalCount': 0
            });
        }

    }
    catch (error) {
        response.error(res, error, 'history_transaction/data');
    }
}

const mainframe_history = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter, customerid } = req.query;
        const dataskip = skip === undefined ? 0 : skip;
        const datatake = take === undefined ? 10 : take;
        const datasort = sort === undefined ? '' : JSON.parse(sort);
        const datafilter = filter === undefined ? '' : filter;

        let orderby = '';
        if (datasort[0] === undefined) {
            orderby = `ORDER BY DateCreate DESC`;
        }
        else {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter !== '') {
            String.prototype.replaceAll = function (search, replacement) {
                let target = this;
                return target.split(search).join(replacement);
            }

            const data1 = datafilter.replaceAll('[', '');
            const data2 = data1.replaceAll(']', '');
            const data3 = data2.replaceAll(',"contains",', ':');
            const data4 = data3.replaceAll(',"or",', ',');
            const data4a = data4.replaceAll(',"=",', ':');
            const data5 = JSON.parse(`{${data4a}}`);

            filtering += 'WHERE ';
            if (data5.TicketNumber !== undefined) {
                filtering += `TicketNumber LIKE '%${data5.TicketNumber}%'`;
            } else if (data5.TicketSourceName !== undefined) {
                filtering += `TicketSourceName LIKE '%${data5.TicketSourceName}%'`;
            } else if (data5.GenesysID !== undefined) {
                filtering += `GenesysID LIKE '%${data5.GenesysID}%'`;
            } else if (data5.ThreadID !== undefined) {
                filtering += `ThreadID LIKE '%${data5.ThreadID}%'`;
            } else if (data5.CategoryName !== undefined) {
                filtering += `CategoryName LIKE '%${data5.CategoryName}%'`;
            } else if (data5.SubCategory1Name !== undefined) {
                filtering += `SubCategory1Name LIKE '%${data5.SubCategory1Name}%'`;
            } else if (data5.SubCategory2Name !== undefined) {
                filtering += `SubCategory2Name LIKE '%${data5.SubCategory2Name}%'`;
            } else if (data5.SubCategory3Name !== undefined) {
                filtering += `SubCategory3Name LIKE '%${data5.SubCategory3Name}%'`;
            } else if (data5.Status !== undefined) {
                filtering += `Status LIKE '%${data5.Status}%'`;
            } else if (data5.ParentNumberID !== undefined) {
                filtering += `ParentNumberID LIKE '%${data5.ParentNumberID}%'`;
            } else if (data5.UserCreate !== undefined) {
                filtering += `UserCreate LIKE '%${data5.UserCreate}%'`;
            } else if (data5.DateCreate !== undefined) {
                filtering += `DateCreate = '${data5.DateCreate}'`;
            }
        }

        const mainframe_history = await knex.raw(`
            SELECT TicketNumber, TicketSourceName, ThreadID,GenesysID, CategoryName, SubCategory1Name, SubCategory2Name, SubCategory3Name, Status, ParentNumberID, UserCreate, DateCreate
            FROM TTICKET WHERE NIK='${customerid}'
            ${filtering} ${orderby}
            OFFSET ${dataskip} ROWS FETCH NEXT ${datatake} ROWS ONLY
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total FROM TTICKET WHERE NIK='${customerid}' ${filtering}`);
        res.json({
            'data': mainframe_history,
            'totalCount': total[0].total
        });
    }
    catch (error) {
        response.error(res, error, 'history_transaction/mainframe');
    }
}

module.exports = {
    data_history,
    mainframe_history,
}
