'use strict';
const knex = require('../config/db_connect');
// const date = require('date-and-time');
const response = require('../helper/json_response');

const report_sla = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter, start_date, end_date } = req.query;
        const dataskip = skip === undefined ? 0 : skip;
        const datatake = take === undefined ? 10 : take;
        const datasort = sort === undefined ? '' : JSON.parse(sort);
        const datafilter = filter === undefined ? '' : filter;

        let orderby = '';
        if (datasort[0] === undefined) {
            orderby = `ORDER BY TicketNumber ASC`;
        }
        else {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter !== '') {
            String.prototype.replaceAll = function (search, replacement) {
                let target = this;
                return target.split(search).join(replacement);
            }

            const data1 = datafilter.replaceAll('[', '');
            const data2 = data1.replaceAll(']', '');
            const data3 = data2.replaceAll(',"contains",', ':');
            const data4 = data3.replaceAll(',"or",', ',');
            const data4a = data4.replaceAll(',"=",', ':');
            const data5 = JSON.parse(`{${data4a}}`);

            filtering += 'AND ';
            if (data5.TicketNumber !== undefined) {
                filtering += `a.TicketNumber LIKE '%${data5.TicketNumber}%'`;
            } else if (data5.CategoryName !== undefined) {
                filtering += `a.CategoryName LIKE '%${data5.CategoryName}%'`;
            } else if (data5.SubCategory1Name !== undefined) {
                filtering += `a.SubCategory1Name LIKE '%${data5.SubCategory1Name}%'`;
            } else if (data5.SubCategory2Name !== undefined) {
                filtering += `a.SubCategory2Name LIKE '%${data5.SubCategory2Name}%'`;
            } else if (data5.SubCategory3Name !== undefined) {
                filtering += `a.SubCategory3Name LIKE '%${data5.SubCategory3Name}%'`;
            } else if (data5.DetailComplaint !== undefined) {
                filtering += `a.DetailComplaint LIKE '%${data5.DetailComplaint}%'`;
            } else if (data5.ResponComplaint !== undefined) {
                filtering += `a.ResponComplaint LIKE '%${data5.ResponComplaint}%'`;
            } else if (data5.StrStatusPelapor !== undefined) {
                filtering += `e.StrStatusPelapor LIKE '%${data5.StrStatusPelapor}%'`;
            } else if (data5.SumberInformasi !== undefined) {
                filtering += `a.SumberInformasi LIKE '%${data5.SumberInformasi}%'`;
            } else if (data5.Status !== undefined) {
                filtering += `a.Status LIKE '%${data5.Status}%'`;
            } else if (data5.DateCreate !== undefined) {
                filtering += `a.DateCreate = '${data5.DateCreate}'`;
            } else if (data5.UserCreate !== undefined) {
                filtering += `a.UserCreate LIKE '%${data5.UserCreate}%'`;
            } else if (data5.UserSolved !== undefined) {
                filtering += `a.UserSolved LIKE '%${data5.UserSolved}%'`;
            } else if (data5.DateSolved !== undefined) {
                filtering += `a.DateSolved = '${data5.DateSolved}'`;
            } else if (data5.DateClose !== undefined) {
                filtering += `a.DateClose = '${data5.DateClose}'`;
            } else if (data5.UserClose !== undefined) {
                filtering += `a.UserClose LIKE '%${data5.UserClose}%'`;
            } else if (data5.SLA !== undefined) {
                filtering += `a.SLA LIKE '%${data5.SLA}%'`;
            } else if (data5.GenesysID !== undefined) {
                filtering += `a.GenesysID LIKE '%${data5.GenesysID}%'`;
            } else if (data5.ThreadID !== undefined) {
                filtering += `a.ThreadID LIKE '%${data5.ThreadID}%'`;
            } else if (data5.NIK !== undefined) {
                filtering += `a.NIK LIKE '%${data5.NIK}%'`;
            } else if (data5.NomorRekening !== undefined) {
                filtering += `a.NomorRekening LIKE '%${data5.NomorRekening}%'`;
            } else if (data5.AccountInbound !== undefined) {
                filtering += `a.AccountInbound LIKE '%${data5.AccountInbound}%'`;
            } else if (data5.AccountID !== undefined) {
                filtering += `a.AccountID LIKE '%${data5.AccountID}%'`;
            } else if (data5.LastResponseBy !== undefined) {
                filtering += `a.LastResponseBy LIKE '%${data5.LastResponseBy}%'`;
            } else if (data5.LastResponseDate !== undefined) {
                filtering += `a.LastResponseDate = '${data5.LastResponseDate}'`;
            } else if (data5.CIF !== undefined) {
                filtering += `a.CIF LIKE '%${data5.CIF}%'`;
            } else if (data5.TicketSourceName !== undefined) {
                filtering += `a.TicketSourceName LIKE '%${data5.TicketSourceName}%'`;
            } else if (data5.SkalaPrioritas !== undefined) {
                filtering += `a.SkalaPrioritas LIKE '%${data5.SkalaPrioritas}%'`;
            } else if (data5.ThreadTicket !== undefined) {
                filtering += `a.ThreadTicket LIKE '%${data5.ThreadTicket}%'`;
            } else if (data5.Amount !== undefined) {
                filtering += `a.Amount LIKE '%${data5.Amount}%'`;
            } else if (data5.Name !== undefined) {
                filtering += `b.Name LIKE '%${data5.Name}%'`;
            } else if (data5.HP !== undefined) {
                filtering += `b.HP LIKE '%${data5.HP}%'`;
            } else if (data5.ORGANIZATION_NAME !== undefined) {
                filtering += `c.ORGANIZATION_NAME LIKE '%${data5.ORGANIZATION_NAME}%'`;
            }
        }

        const report_sla = await knex.raw(`
            SELECT ROW_NUMBER()over(order by TicketNumber) as [No],*,
            CASE
                WHEN UsedDaySLA < 0 THEN 'Over ' + CONVERT(varchar(10), UsedDaySLA * -1) + ' Day'
                ELSE CONVERT(varchar(10), UsedDaySLA) + ' Day Later'
            END AS UsedDaySLAOK
            FROM (
                SELECT
                    a.TicketNumber,
                    a.CategoryName,
                    a.SubCategory1Name,
                    a.SubCategory2Name,
                    a.SubCategory3Name,
                    a.DetailComplaint,
                    a.ResponComplaint,
                    a.SumberInformasi,
                    a.Status,
                    a.DateCreate,
                    a.UserCreate,
                    a.UserSolved,
                    a.DateSolved,
                    a.DateClose,
                    a.UserClose,
                    a.SLA,
                    a.GenesysID,
                    a.ThreadID,
                    a.NIK,
                    a.NomorRekening,
                    a.AccountInbound,
                    a.AccountID,
                    a.LastResponseBy,
                    a.LastResponseDate,
                    b.CIF,
                    a.TicketSourceName,
                    a.SkalaPrioritas,
                    a.ThreadTicket,
                    a.Amount,
                    CASE
                        WHEN a.Status = 'Closed' THEN [dbo].[fn_GetTotalWorkingDays](a.DateSolved, d.DateClose) - 1
                        WHEN a.Status = 'Solved' THEN [dbo].[fn_GetTotalWorkingDays](a.DateSolved, d.DateClose) - 1
                        ELSE [dbo].[fn_GetTotalWorkingDays](GETDATE(), d.DateClose) - 1
                    END AS UsedDaySLA,
                    b.Name,
                    b.HP,
                    c.ORGANIZATION_NAME,
                    -- e.StrStatusPelapor
                    (select top 1 StrStatusPelapor from BTN_trxTticket where BTN_trxTticket.TicketNumber=a.TicketNumber) as StrStatusPelapor
                FROM
                    tTicket a
                    LEFT OUTER JOIN mCustomer b ON b.CustomerID = a.NIK
                    LEFT OUTER JOIN mOrganization c ON c.ORGANIZATION_ID = a.Divisi
                    LEFT OUTER JOIN tCloseTicket d ON d.TicketNumber = a.TicketNumber
                    -- LEFT OUTER JOIN BTN_trxTticket e ON e.TicketNumber = a.TicketNumber
                WHERE 
                    a.DateCreate Between '${start_date} 00:00:00'
                    and '${end_date} 23:59:00'
                    and a.TicketNumber <> ''
                    ${filtering} 
            ) as R_SLA 
            ${orderby}
            OFFSET ${dataskip} ROWS FETCH NEXT ${datatake} ROWS ONLY
        `);
        const total = await knex.raw(`
            SELECT count(*) AS total FROM tTicket a
            LEFT OUTER JOIN mCustomer b ON b.CustomerID = a.NIK
            LEFT OUTER JOIN mOrganization c ON c.ORGANIZATION_ID = a.Divisi
            LEFT OUTER JOIN tCloseTicket d ON d.TicketNumber = a.TicketNumber
            -- LEFT OUTER JOIN BTN_trxTticket e ON e.TicketNumber = a.TicketNumber
            WHERE a.DateCreate Between '${start_date} 00:00:00'and '${end_date} 23:59:00' and a.TicketNumber <> ''
            ${filtering}
        `);
        res.json({
            'status': 200,
            'data': report_sla,
            'totalCount': total[0].total
        });
    }
    catch (error) {
        response.error(res, error, 'report/sla');
    }
}

const report_sla_export = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { start_date, end_date } = req.query;
        const a = new Date(start_date);
        const b = new Date(end_date);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;

        if (days <= 10) {
            const report_sla = await knex.raw(`
                SELECT ROW_NUMBER()over(order by TicketNumber) as [No],*,
                CASE
                    WHEN UsedDaySLA < 0 THEN 'Over ' + CONVERT(varchar(10), UsedDaySLA * -1) + ' Day'
                    ELSE CONVERT(varchar(10), UsedDaySLA) + ' Day Later'
                END AS UsedDaySLAOK
                FROM (
                    SELECT
                        a.TicketNumber,
                        a.CategoryName,
                        a.SubCategory1Name,
                        a.SubCategory2Name,
                        a.SubCategory3Name,
                        a.DetailComplaint,
                        a.ResponComplaint,
                        a.SumberInformasi,
                        a.Status,
                        convert(varchar, a.DateCreate, 120) as DateCreate,
                        a.UserCreate,
                        convert(varchar, a.DateSolved, 120) as DateSolved,
                        a.UserSolved,
                        convert(varchar, a.DateClose, 120) as DateClose,
                        a.UserClose,
                        a.SLA,
                        a.GenesysID,
                        a.ThreadID,
                        a.NIK,
                        a.NomorRekening,
                        a.AccountInbound,
                        a.AccountID,
                        a.LastResponseBy,
                        convert(varchar, a.LastResponseDate, 120) as LastResponseDate,
                        b.CIF,
                        a.TicketSourceName,
                        a.SkalaPrioritas,
                        a.ThreadTicket,
                        a.Amount,
                        CASE
                            WHEN a.Status = 'Closed' THEN [dbo].[fn_GetTotalWorkingDays](a.DateSolved, d.DateClose) - 1
                            WHEN a.Status = 'Solved' THEN [dbo].[fn_GetTotalWorkingDays](a.DateSolved, d.DateClose) - 1
                            ELSE [dbo].[fn_GetTotalWorkingDays](GETDATE(), d.DateClose) - 1
                        END AS UsedDaySLA,
                        b.Name,
                        b.HP,
                        c.ORGANIZATION_NAME,
                        -- e.StrStatusPelapor
                        (select top 1 StrStatusPelapor from BTN_trxTticket where BTN_trxTticket.TicketNumber=a.TicketNumber) as StrStatusPelapor
                    FROM
                        tTicket a
                        LEFT OUTER JOIN mCustomer b ON b.CustomerID = a.NIK
                        LEFT OUTER JOIN mOrganization c ON c.ORGANIZATION_ID = a.Divisi
                        LEFT OUTER JOIN tCloseTicket d ON d.TicketNumber = a.TicketNumber
                        -- LEFT OUTER JOIN BTN_trxTticket e ON e.TicketNumber = a.TicketNumber
                    WHERE 
                        a.DateCreate Between '${start_date} 00:00:00'
                        and '${end_date} 23:59:00'
                        and a.TicketNumber <> ''
                ) as R_SLA 
                ORDER BY TicketNumber ASC
            `);

            res.json({
                'status': 200,
                'data': report_sla
            });
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range 10 days.`
            });
            res.end();
        }
    }
    catch (error) {
        response.error(res, error, 'report/sla_export');
    }
}

module.exports = {
    report_sla,
    report_sla_export
}
