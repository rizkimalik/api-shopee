const express = require('express')
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const port = process.env.PORT || 3001;

dotenv.config();
express.application.prefix = express.Router.prefix = function (path, configure) {
    const router = express.Router();
    this.use(path, router);
    configure(router);
    return router;
}

const app = express();
const server = http.createServer(app);


//?parse application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

//? routes api endpoint
const api = require('./routes');
api(app);

server.listen(port, () => {
    console.log(`✨Server app listening at port : 🚀 http://localhost:${port}`)
})
