'use strict';
const customer = require('./controller/customer');
const account_number = require('./controller/account_number');
const channel = require('./controller/customer_channel');
const customer_group = require('./controller/customer_group');
const history_transaction = require('./controller/history_transaction');
const report = require('./controller/report');

module.exports = function (app) {
    app.route('/').get(function (req, res) {
        res.json({ message: "Application API is running! 🤘🚀" });
        res.end();
    });
    // app.route('/menu').get(menu.menu);
    app.prefix('/customer',  function (api) {
        api.route('/data').get(customer.data_customers);
        api.route('/insert').post(customer.insert_customer);
        api.route('/update').put(customer.update_customer);
        api.route('/delete').delete(customer.delete_customer);
    });
    
    app.prefix('/account_number',  function (api) {
        api.route('/:CustomerID').get(account_number.data_accountnumber);
        api.route('/insert').post(account_number.insert_accountnumber);
        api.route('/update').put(account_number.update_accountnumber);
        api.route('/delete').delete(account_number.delete_accountnumber);
    });
    
    app.prefix('/customer_channel',  function (api) {
        api.route('/').get(channel.all_channel);
        api.route('/:CustomerID').get(channel.data_channel);
        api.route('/insert').post(channel.insert_channel);
        api.route('/update').put(channel.update_channel);
        api.route('/delete').delete(channel.delete_channel);
    });
    
    app.prefix('/customer_group',  function (api) {
        api.route('/update').post(customer_group.update_group);
        api.route('/delete').post(customer_group.delete_group);
    });
    
    app.prefix('/history_transaction',  function (api) {
        api.route('/data').get(history_transaction.data_history);
        api.route('/mainframe').get(history_transaction.mainframe_history);
    });
    
    app.prefix('/report',  function (api) {
        api.route('/sla').get(report.report_sla);
        api.route('/sla_export').get(report.report_sla_export);
    });
}
