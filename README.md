# api-mendawai
### Web Config Setting :

localhost:4000 (root folder)

localhost:88/ApiShopee (config/temp folder)



### Verbs Methods

https://stackoverflow.com/questions/12440277/how-do-i-enable-http-put-and-delete-for-asp-net-mvc-in-iis

```
<verbs allowUnlisted="false">
    <add verb="GET" allowed="true" />
    <add verb="POST" allowed="true" />
    <add verb="DELETE" allowed="true" />
    <add verb="PUT" allowed="true" />
</verbs>
```


